<?php
/**
 * @link http://zenothing.com/
 *
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use app\models\Block;
use yii\grid\GridView;
use yii\helpers\Html;

echo Html::tag('h1', Yii::t('app', 'Block by IP'));
echo Html::a(Yii::t('app', 'Create'), ['edit'], ['class' => 'btn btn-primary']);
echo '<br/><br/>';

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'ip',
        'reason',
        'time:datetime',
        [
            'label' => Yii::t('app', 'Action'),
            'format' => 'html',
            'contentOptions' => ['class' => 'action'],
            'value' => function(Block $model) {
                return implode(' ', [
                    Html::a('', ['edit', 'ip' => $model->ip], ['class' => 'glyphicon glyphicon-pencil']),
                    Html::a('', ['delete', 'ip' => $model->ip], ['class' => 'glyphicon glyphicon-trash'])
                ]);
            }
        ]
    ]
]);
