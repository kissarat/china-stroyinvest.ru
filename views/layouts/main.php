<?php
/**
 * @link http://zenothing.com/
 */

use app\controllers\HomeController;
use app\helpers\MainAsset;
use app\modules\article\controllers\ArticleController;
use app\widgets\Alert;
use app\widgets\Ext;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"><!--

Домен зарегистрирован на имя разработчика и хоститься на его (разработчика) серверах,
но разработчик не являеться владельцем сайта и не несет ответственности за его действия,
т.е. разработчик не несет ответственности за действия владельца сайта,
которые, в том числе, включают в себя проведения любых денежных операций
связанных с кошельком Perfect Money U9060584
<?= "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ?>
-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/images/cover.png" />
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body class="<?= implode(' ', $route) ?>">
<header>
    <div class="container">
        <img class="logo" src="/images/logo.png" alt="Logotype" />
        <div class="contact">
            <div class="basket">
                <?php
                $items = Yii::$app->user->getIsGuest() ? [
                    Html::a(Yii::t('app', 'Login'), ['/user/login'], ['class' => 'cabinet button']),
                    Html::a(Yii::t('app', 'Signup'), ['/user/signup'], ['class' => 'cabinet button']),
                ]: [
                    Html::a(Yii::t('app', 'Logout'), ['/user/logout'], ['class' => 'cabinet button']),
                    Html::a(Yii::t('app', 'Profile'), ['/user/view'], ['class' => 'cabinet button']),
                ];

                $items[] = 'ru' == Yii::$app->language
                    ? Html::a(Yii::t('app', 'EN'), ['/lang/lang/choice', 'code' => 'en'], [
                        'title' => 'English',
                        'class' => 'cabinet button'
                    ])
                    : Html::a(Yii::t('app', 'RU'), ['/lang/lang/choice', 'code' => 'ru'], [
                        'title' => 'Русский',
                        'class' => 'cabinet button'
                    ]);

                echo implode("\n", array_reverse($items));
                ?>
            </div>
            <div class="review">
                <?= Html::a(Html::img('/images/review.png', [
                    'alt' => Yii::t('app', 'Reviews')
                ]), ['/review/index']) ?>
            </div>
        </div>
        <!--div id="cover">
            <img src="/images/cover.svg" alt="Cover" />
        </div-->
    </div>
</header>
<?php $this->beginBody() ?>
<div class="wrap <?= $login ?>">
    <?php
    NavBar::begin();

    $items = [
        ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
        ['label' => Yii::t('app', 'Marketing'), 'url' => ['/article/article/page', 'name' => 'marketing']],
        ['label' => Yii::t('app', 'About products'), 'url' => ['/article/article/page', 'name' => 'products']],
        ['label' => Yii::t('app', 'News'), 'url' => ['/article/article/index']],
        ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/feedback/create']]
    ];

    if ($manager) {
        $items[0] = ['label' => Yii::t('app', 'Admin Panel'),
            'url' => 'http://admin.' . $_SERVER['HTTP_HOST'] . '/bank/node/index',
            'options' => [
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'auth' => Yii::$app->user->identity->auth
                    ]
                ]
            ]
        ];
    }

    $items[] = ['label' => Yii::t('app', 'Contacts'), 'url' => ['/article/article/page', 'name' => 'contacts']];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <form class="search">
            <input type="search" readonly />
            <button type="button"></button>
        </form>

        <div class="main">
            <aside>
                <div class="statistics">
                    <h2><?= Yii::t('app', 'Statistics') ?></h2>
                    <?php
                    if ($this->beginCache('statistics', ['duration' => 10])) {
                        echo Yii::$app->view->render('@app/views/home/statistics', [
                            'statistics' => HomeController::statistics()
                        ]);
                        $this->endCache();
                    }
                    ?>
                    <img src="/images/under.png" alt="Under statistics image" />
                </div>
                <?php if (!('article' == $route[0] && 'index' == $route[1])): ?>
                    <div class="news">

                        <?php
                        if ($this->beginCache('news', ['duration' => 10])) {
                            echo Yii::$app->view->render('@app/modules/article/views/article/index', ArticleController::news());
                            $this->endCache();
                        }
                        ?>

                        <?= Html::a(Html::img('/images/more.png'), ['/article/article/index']) ?>
                    </div>
                <?php endif ?>
                <div class="advices">
                    <h2>Полезные советы</h2>
                    <ol>
                        <li>Пройдите процедуру регистрации</li>
                        <li>Пополните баланс в личном кабинете</li>
                        <li>Откройте депозит</li>
                        <li>Получайте прибыль</li>
                    </ol>
                </div>
                <div class="fraud">
                    <img src="/images/fraud.png" alt="КПД" />
                    <div>Клуб профессионального доверия</div>
                </div>
            </aside>
            <div>
                <?= Breadcrumbs::widget([
                    'homeLink' => false,
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>

    <div id="linux">
        <div>
            <img src="/images/linux.png" alt="Linux" />
        </div>
        <?= Html::tag('div', Yii::t('app', 'Welcome, Linux user. We are glad you use open source software!'), [
            'class' => 'welcome'
        ]) ?>
        <div class="glyphicon glyphicon-remove"></div>
    </div>

    <footer class="container">
        <div class="skype">
            <a href="skype:yaroslav.isaev888">Skype Админа</a>
            <span>+7-968-733-49-37</span>
            <a href="skype:andrey-piter83">Skype Модератора</a>
            <span>chinastroyinvest@gmail.com</span>
            <a href="skype:?chat&blob=ILYFJBz4-Ak5R0rVNw769aoCD8pzPISQzF_LnbwALK2XhHjfIeUvXMq-_m5PE2ux0fRonFg6KKcdrMfnh-r-zn4">Skype-чат</a>
        </div>
        <?= Ext::social() ?>
        <div id="metrika">
            <!-- Yandex.Metrika informer -->
            <a href="https://metrika.yandex.ru/stat/?id=32410265&amp;from=informer"
               target="_blank" rel="nofollow">
                <img src="https://informer.yandex.ru/informer/32410265/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                     alt="Яндекс.Метрика"
                     title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" />
            </a>
            <!-- /Yandex.Metrika informer -->
            <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fchina-stroyinvest.ru%2Fmain.css&amp;profile=css3&amp;usermedium=all">
                <img style="border:0;width:88px;height:31px"
                     src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
                     alt="Правильный CSS!" />
            </a>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
