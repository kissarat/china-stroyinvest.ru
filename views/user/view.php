<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */


$this->title = $model->name;
if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$columns = [
    'name',
    'email:email',
    [
        'attribute' => 'account',
        'format' => 'html',
        'value' => Html::tag('span', $model->account) . ' ' . ($model->account > 0 ? Html::a(Yii::t('app', 'Withdraw'),
                ['invoice/invoice/create', 'amount' => (int) $model->account, 'scenario' => 'withdraw'],
                ['class' => 'btn btn-success btn-xs']) : '')
    ],
    'phone',
    'skype',
    'forename',
    'surname',
    'perfect',
];

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    $columns[] = [
        'attribute' => 'status',
        'value' => User::statuses()[$model->status]
    ];
}

$referral = Url::to(['/user/signup', 'ref_name' => $model->name], true);
?>
<div class="roompic"></div>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    require 'panel.php';
    ?>

    <div>
        <div>
            <?php
            echo Html::a(Yii::t('app', 'Referral Link'), $referral, ['class' => 'form-label']);
            ?>

            <input class="form-control" value="<?= Url::to($referral, true); ?>">
        </div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => $columns,
        ]) ?>
    </div>
</div>
