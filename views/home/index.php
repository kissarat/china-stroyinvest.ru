<?php
/**
 * @link http://zenothing.com/
 * @var string $statistics
 */

use app\helpers\SliderAsset;
use app\modules\article\models\Article;
use yii\helpers\Html;

$this->title = Yii::$app->name;

SliderAsset::register($this);
$slides = [];
for($i = 1; $i <= 7; $i++) {
    $slides[] = Html::img("/images/slides/$i.jpg");
}
?>
<div class="home-index">
    <?= Html::tag('div', Html::ul($slides, [
        'encode' => false
    ]), ['class' => 'slides']) ?>

    <?= Yii::$app->view->renderFile('@app/modules/article/views/article/view.php', [
        'model' => Article::findOne(['name' => 'about'])
    ]) ?>
</div>
