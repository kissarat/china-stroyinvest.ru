<?php
/**
 * @link http://zenothing.com/
*/
use yii\helpers\Html;
?>
<div class="content">
<?php
$items = [];
foreach($statistics as $name => $value) {
    $items[] = Html::tag('span', Yii::t('app', $name) . ':') . Html::tag('span', $value);
}

$items[] = 'Вклады от 10 до 3000$ сроком на 5 рабочих дней под 40%';
$items[] = 'Реферальный бонус: 5%';
$items[] = 'Вывод:  с 10:00 до 22:00 мск. Воскресенье выходной';
$items[] = 'Платежные системы:  Perfect Money';

echo Html::ul($items, [
    'encode' => false
]);
?>
</div>
