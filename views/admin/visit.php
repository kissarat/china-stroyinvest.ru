<?php
/**
 * @link http://zenothing.com/
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var string $user
 */

use app\models\Visit;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Visits');
if ($user) {
    $this->title .= ': ' . $user;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visits'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $user;
}

echo Html::tag('h1', $this->title);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'time:datetime',
        [
            'attribute' => 'spend',
            'value' => function(Visit $model) {
                $interval = new DateInterval('PT' . floor($model->spend / 60) . 'M' . ($model->spend % 60) . 'S');
                return $interval->format('%I:%S');
            }
        ],
        [
            'attribute' => 'user_name',
            'format' => 'html',
            'value' => function(Visit $model) {
                return $model->user_name ? Html::a($model->user_name, ['visit', 'user' => $model->user_name]) : null;
            }
        ],
        [
            'attribute' => 'path',
            'format' => 'html',
            'value' => function(Visit $model) {
                $path = '/' . $model->path;
                $origin = 'http://' . substr($_SERVER['HTTP_HOST'], 6);
                return Html::a($path, $origin . $path, ['class' => '_blank']);
            }
        ],
        [
            'attribute' => 'ip',
            'format' => 'html',
            'contentOptions' => [
                'title' => Yii::t('app', 'Block')
            ],
            'value' => function(Visit $model) {
                return Html::a($model->ip, ['/block/edit', 'ip' => $model->ip]);
            }
        ],
        [
            'attribute' => 'agent',
            'format' => 'html',
            'contentOptions' => [
                'class' => 'small'
            ],
            'value' => function(Visit $model) {
                $agent = str_replace('Mozilla/5.0', '', $model->agent);
                $agent = preg_replace('/AppleWebKit\/\d+\.\d+ \(KHTML, like Gecko\)/', '', $agent);
                return preg_replace('/(Windows|Linux|Android|Firefox|Chrome)/', '<strong>$1</strong>', $agent);
            }
        ]
    ]
]);
