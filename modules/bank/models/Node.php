<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\bank\models;

use app\helpers\Account;
use app\models\Message;
use app\models\User;
use Exception;
use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "node".
 *
 * @property integer $id
 * @property string $user_name
 * @property number $amount
 * @property integer $time
 *
 * @property User $user
 * @property Node[] $children
 */
class Node extends ActiveRecord
{
    public static function tableName() {
        return 'node';
    }

    public function rules() {
        return [
            [['user_name', 'amount', 'time', 'datetime'], 'required'],
            [['user_name'], 'string', 'max' => 24],
            ['time', 'default', 'value' => $_SERVER['REQUEST_TIME']],
            ['amount', 'number', 'min' => 10, 'max' => 3000]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_name' => Yii::t('app', 'Username'),
            'amount' => Yii::t('app', 'Amount'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->hasOne(User::class, ['name' => 'user_name']);
    }

    public function setUser(User $value) {
        $this->user_name = $value->name;
    }

    public function invest() {
        $this->time = $_SERVER['REQUEST_TIME'];
        $amount = $this->amount;
        if ($this->user->ref_name) {
            $bonus = $amount * 0.05;
            $this->user->referral->account += $bonus;
            if ($this->user->referral->save()) {
                Message::send($this->user_name, 'info', 'You received a bonus');
                $amount -= $bonus;
            }
        }
        Account::add('profit', $amount);
        return $this->save();
    }

    public function __debuginfo() {
        $bundle = [
            'id' => $this->id,
            'time' => date($this->time),
            'user_name' => $this->user_name,
            'account' => $this->user->account
        ];
        if (count($this->errors) > 0) {
            $bundle['errors'] = $this->errors;
        }
        if (count($this->user->errors) > 0) {
            $bundle['user_errors'] = $this->user->errors;
        }
        return json_encode($bundle, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function __toString() {
        return '#' . $this->id . ' $' . $this->amount;
    }

    public function getIncome() {
        return $this->amount * 1.4;
    }

    public function getDatetime() {
        return date('Y-m-d H:i', $this->time);
    }

    public function setDatetime($value) {
        $this->time = strtotime($value);
    }
}
