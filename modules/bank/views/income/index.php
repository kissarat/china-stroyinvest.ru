<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use app\modules\bank\models\Income;
use app\modules\bank\models\Node;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View
 * @var $searchModel \app\modules\bank\models\search\Income
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Yii::t('app', 'Income');
$columns = [
    'id',
    'node_id',
    [
        'attribute' => 'amount',
        'value' => function($model) {
            $amount = $model->amount;
            $ref_name = $model->user->ref_name;
            if ($ref_name) {
                $amount .= " (+ $" . ($amount * 0.05) . " бонус $ref_name)";
            }
            return $amount;
        }
    ]
];

if (!$user) {
    $columns[] = [
        'attribute' => 'user_name',
        'format' => 'html',
        'value' => function(Income $model) {
            return Html::a($model->user_name, ['/user/view', 'name' => $model->user_name]);
        }
    ];
}

$columns[] = 'time:datetime';
?>
<div class="archive-index">

    <?php
    if ($user) {
        echo Yii::$app->view->render('@app/views/user/panel', [
            'model' => User::findOne(['name' => $user])
        ]);
    }
    ?>

    <div>
        <h1 class="bagatelle"><?= Html::encode($this->title) ?></h1>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{pager}\n{errors}\n{summary}\n{items}",
            'columns' => $columns,
        ]); ?>
    </div>
</div>
