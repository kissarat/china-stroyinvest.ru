<?php
/**
 * @link http://zenothing.com/
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\bank\models\Node */

$this->title = Yii::t('app', 'Update');
$url = ['index'];
if (Yii::$app->user->identity->isManager()) {
    $url['min'] = 0;
}
else {
    $url['user'] = Yii::$app->user->identity->name;
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deposits'), 'url' => $url];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="node-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
