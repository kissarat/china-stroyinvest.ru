<?php

require_once __DIR__ . '/../config/boot.php';

$name = $config['components']['db']['username'];
$password = $config['components']['db']['password'];

pg_connect('host=/var/run/postgresql');
pg_query("CREATE USER \"$name\" PASSWORD '$password'");
pg_query("CREATE DATABASE \"$name\" OWNER \"$name\"");
pg_close();

$schema = file_get_contents(ROOT . '/config/schema.sql');
$schema = explode(';', $schema);
$pdo = new PDO('pgsql:host=localhost;dbname=' . $name, $name, $password);
$pdo->beginTransaction();
foreach($schema as $sql) {
    $pdo->exec($sql);
}
$pdo->commit();
